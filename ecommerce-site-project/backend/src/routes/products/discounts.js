// src/routes/products/discounts.js

const Medusa = require('@medusajs/medusa');

const productsModule = new Medusa.Modules.ProductsService();

const DiscountsRoute = {
  path: '/products/:productId/discounts',
  method: 'get',
  async handler(req, res) {
    const { productId } = req.params;

    const product = await productsModule.getProductById(productId);
    const discounts = await productsModule.getProductDiscounts(productId);

    res.json({
      product,
      discounts,
    });
  },
};

module.exports = DiscountsRoute;
