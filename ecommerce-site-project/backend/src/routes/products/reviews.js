
const Medusa = require('@medusajs/medusa');

const productsModule = new Medusa.Modules.ProductsService();

const ReviewsRoute = {
  path: '/products/:productId/reviews',
  method: 'get',
  async handler(req, res) {
    const { productId } = req.params;

    const product = await productsModule.getProductById(productId);
    const reviews = await productsModule.getProductReviews(productId);

    res.json({
      product,
      reviews,
    });
  },
};

module.exports = ReviewsRoute;
