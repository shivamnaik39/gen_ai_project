import { makeRequest } from '@medusajs/medusa';

const productId = 1;

describe('/products/:productId/reviews', () => {
  it('should return reviews for product with valid ID', async () => {
    const response = await makeRequest('GET', `/products/${productId}/reviews`);

    const reviews = response.json();

    expect(reviews).toBeInstanceOf(Array);
    expect(reviews.length).toBeGreaterThan(0);
  });

  it('should return 404 for product with invalid ID', async () => {
    const invalidProductId = 1000;

    const response = await makeRequest('GET', `/products/${invalidProductId}/reviews`);

    expect(response.status).toBe(404);
  });

  it('should return empty array for product with no reviews', async () => {
    const productIdWithNoReviews = 2;

    const response = await makeRequest('GET', `/products/${productIdWithNoReviews}/reviews`);

    expect(response.json()).toEqual([]);
  });
});