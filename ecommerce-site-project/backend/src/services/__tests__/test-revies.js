// Test case 1: Get reviews for product with valid ID
const productId = 1;

const response = await makeRequest('GET', `/products/${productId}/reviews`);

const reviews = response.json();

expect(reviews).toBeInstanceOf(Array);
expect(reviews.length).toBeGreaterThan(0);

// Test case 2: Get reviews for product with invalid ID
const invalidProductId = 1000;

const response = await makeRequest('GET', `/products/${invalidProductId}/reviews`);

expect(response.status).toBe(404);

// Test case 3: Get reviews for product with no reviews
const productIdWithNoReviews = 2;

const response = await makeRequest('GET', `/products/${productIdWithNoReviews}/reviews`);

expect(response.json()).toEqual([]);