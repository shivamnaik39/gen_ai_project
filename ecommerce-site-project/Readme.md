# E-commerce Project

To run this application you will need 
* node >= 16.x.x installed
* postgresql database

This project consist of 2 folders
* backend 
* frontend (Next.js storefront)

## Getting Started

First start postgresql database and make sure it is running properly


Then navigate into the backend folder and install dependencies using
```
yarn install
```
Start the backend server using the command:
```
yarn run start
```

Then navigate into the Next.js frontend folder and install dependencies using
```
yarn install
```

Start the Next JS frontend server using the command:
```
yarn run start
```


Backend will be served at http://localhost:7001/

Frontend will be served at http://localhost:8000/ 
