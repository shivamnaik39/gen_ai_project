import React from 'react';
import { useMedusa } from '@medusajs/react';

const ProductReviews = ({ productId }) => {
  const { reviews } = useMedusa(productId);

  return (
    <div>
      <h1>Reviews for Product {productId}</h1>
      <ul>
        {reviews.map((review) => (
          <li key={review.id}>
            <h2>{review.title}</h2>
            <p>{review.body}</p>
            <p>Rating: {review.rating}</p>
          </li>
        ))}
      </ul>
    </div>
  );
};

export default ProductReviews;
