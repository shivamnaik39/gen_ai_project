import React from 'react';
import { render } from '@testing-library/react';

import ProductReviews from './reviews';

const productId = 1;

describe('ProductReviews', () => {
  it('should display reviews for product with valid ID', () => {
    const wrapper = render(<ProductReviews productId={productId} />);

    expect(wrapper.find('h1').text()).toBe('Reviews for Product 1');
    expect(wrapper.find('ul').children().length).toBeGreaterThan(0);
  });

  it('should display no reviews for product with invalid ID', () => {
    const invalidProductId = 1000;

    const wrapper = render(<ProductReviews productId={invalidProductId} />);

    expect(wrapper.find('h1').text()).toBe('Reviews for Product 1000');
    expect(wrapper.find('ul').children().length).toBe(0);
  });

  it('should display no reviews for product with no reviews', () => {
    const productIdWithNoReviews = 2;

    const wrapper = render(<ProductReviews productId={productIdWithNoReviews} />);

    expect(wrapper.find('h1').text()).toBe('Reviews for Product 2');
    expect(wrapper.find('ul').children().length).toBe(0);
  });
});