import React from 'react';
import { useMedusa } from '@medusajs/react';

const ProductDiscounts = ({ productId }) => {
  const { discounts } = useMedusa(productId);

  return (
    <div>
      <h1>Discounts for Product {productId}</h1>
      <ul>
        {discounts.map((discount) => (
          <li key={discount.id}>
            <h2>{discount.name}</h2>
            <p>{discount.description}</p>
            <p>Code: {discount.code}</p>
          </li>
        ))}
      </ul>
    </div>
  );
};

export default ProductDiscounts;